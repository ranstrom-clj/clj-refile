(ns build
  (:require [clojure.tools.build.api :as b]))

(def lib 'com.gitlab.ranstrom/clj-refile)
(def version (format "0.1.%s" (b/git-count-revs nil)))
(def class-dir "target/classes")
(def basis (b/create-basis {:project "deps.edn"}))
(def jar-file (format "target/%s-%s.jar" (name lib) version))

(defn clean [_]
  (b/delete {:path "target"}))

(defn jar [_]
  (b/write-pom {:class-dir class-dir
                :lib lib
                :version version
                :basis basis
                :src-dirs ["src"]})
  (b/copy-dir {:src-dirs ["src"] ;; resources
               :target-dir class-dir})
  (b/jar {:class-dir class-dir
          :jar-file jar-file}))

;; Doesn't work due to GitLab headers.

(defn deploy [_]
  (let [dd-deploy (try (requiring-resolve 'deps-deploy.deps-deploy/deploy) (catch Throwable _))]
    (if dd-deploy
      (dd-deploy {:repository {"gitlab-maven" {:url "https://gitlab.com/api/v4/projects/20453419/packages/maven"
                                               :username "Private-Token"
                                               :password "<TOKEN-HERE>"}}
                  :installer  :remote
                  :artifact   jar-file
                  :pom-file   (b/pom-path {:lib       lib
                                           :class-dir class-dir})})
      (throw (ex-info "deploy error in build-clj" {})))))
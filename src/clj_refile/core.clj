(ns clj-refile.core
  (:require [cheshire.core :as c]
            [clj-refile.parse :as p]
            [clj-refile.providers.azure-blob :refer [map->AzureBlobFile]]
            [clj-refile.providers.azure-data-lake :refer [map->AzureDataLakeFile]]
            [clj-refile.providers.azure-file :refer [map->AzureFile]]
            [clj-refile.providers.ftp :refer [map->FTPFile]]
            [clj-refile.providers.http :refer [map->HTTPFile]]
            [clj-refile.providers.local :refer [map->File]]
            [clj-refile.providers.sftp :refer [map->SFTPFile]]
            [clj-refile.providers.smartsheet :refer [map->SmartsheetFile]]
            [clojure.core.async :refer [>! <! go-loop chan close! <!!]]
            [clojure.edn :as edn]
            [clojure.java.io :as io]
            [clojure.string :as string]
            [taoensso.timbre :as log])
  (:import (clj_refile.protocols FileProtocol)
           (java.io FileInputStream)
           (java.util UUID)
           (java.util.zip GZIPInputStream)))

(defn uuid [] (str (UUID/randomUUID)))

(defn- scheme->FileProtocol [scheme]
  (case (keyword scheme)
    :azure-blob      map->AzureBlobFile
    :azure-data-lake map->AzureDataLakeFile
    :azure-file      map->AzureFile
    :ftp             map->FTPFile
    :http            map->HTTPFile
    :https           map->HTTPFile
    :local           map->File
    :sftp            map->SFTPFile
    :smartsheet      map->SmartsheetFile
    (throw (Exception. (str "Unsupported scheme " scheme)))))

(defn- get-uri-scheme
  [filepath]
  (let [uri-parsed (p/parse-uri filepath)
        uri-host (or (:host uri-parsed) "")
        uri-scheme (-> uri-parsed :scheme p/lower-case)]
    (cond
      (or (nil? uri-scheme) (some #(= uri-scheme %) ["file" "c"])) :local
      (string/ends-with? uri-host "blob.core.windows.net") :azure-blob
      (string/ends-with? uri-host "dfs.core.windows.net") :azure-data-lake
      (string/ends-with? uri-host "file.core.windows.net") :azure-file
      (string/ends-with? uri-host "api.smartsheet.com") :smartsheet
      :else (keyword uri-scheme))))

(defn ->file
  [filepath & [{:keys [scheme conn-props]}]]
  (let [uri-scheme (or scheme (get-uri-scheme filepath))]
    ((scheme->FileProtocol uri-scheme)
     {:filepath   filepath
      :scheme     uri-scheme
      :conn-props conn-props})))

(defn- download&upload [^FileProtocol input ^FileProtocol output]
  (let [temp (->file (str "./" (uuid)))]
    (.download input temp)
    (.upload output temp)
    (.delete-file temp)
    output))

(def schemes-with-copy [:local])

(defn- copyable? [^FileProtocol input ^FileProtocol output]
  (let [schemes [(:scheme input) (:scheme output)]]
    (and (some #{(:scheme input)} schemes-with-copy)
         (apply = schemes)
         (or (every? #{:local} schemes)
             (= (:conn-props input) (:conn-props output))))))

(defn copy [^FileProtocol input ^FileProtocol output]
  (let [schemes [(:scheme input) (:scheme output)]
        copy-fn (cond
                  (copyable? input output) #(.copy %1 %2)
                  (not-any? #{:local} schemes) download&upload
                  (= :local (:scheme output)) #(.download %1 %2)
                  (= :local (:scheme input)) #(.upload %2 %1)
                  :else (throw (Exception. (str "Unhandled scenario for input " input
                                                " and output " output))))]
    (copy-fn input output)))

(defn file->map
  ([filepath]
   (file->map filepath {}))
  ([filepath {:keys [edn-readers]}]
   (let [type (-> filepath p/get-file-extension string/lower-case keyword)]
     (case type
       :manifest (c/parse-string (slurp filepath) true)
       :json (c/parse-string (slurp filepath) true)
       :edn (if edn-readers
              (edn/read-string {:readers edn-readers} (slurp filepath))
              (edn/read-string (slurp filepath)))
       :else (throw (Exception. (str "File type " type " unsupported for " filepath)))))))

(defn map->json-file [filepath data]
  (try
    (log/debug "Writing to path: " filepath ", data:\n" data)
    (->> filepath io/writer (c/generate-stream data))
    filepath
    (catch Exception e
      (throw (Exception. (str "Error writing map to json: " e))))))

; Count lines borrowed from:
; https://gist.github.com/atroche/08b966664489f80e87a145cef6775b07

(def ^:private one-meg (* 1024 1024))

(defn- count-newlines [^bytes barray]
  (let [num-bytes (alength barray)]
    (loop [i 0
           newlines 0]
      (if (>= i num-bytes)
        newlines
        (if (= 10 (aget ^bytes barray i))
          (recur (inc i)
                 (inc newlines))
          (recur (inc i)
                 newlines))))))

(defn- count-file-rows-async
  "Performance file counter over large data-sets"
  [filepath gzip]
  (with-open [file-stream (-> filepath FileInputStream. (#(if gzip (GZIPInputStream. %) %)))]
    (let [channel (chan 500)
          counters (for [_ (range 4)]
                     (go-loop [newline-count 0]
                       (let [barray (<! channel)]
                         (if (nil? barray)
                           newline-count
                           (recur (+ newline-count (count-newlines barray)))))))]
      (go-loop []
        (let [barray (byte-array one-meg)
              bytes-read (.read file-stream barray)]
          (>! channel barray)
          (if (pos? bytes-read) (recur) (close! channel))))
      (reduce + (map <!! counters)))))

(defn count-rows [filepath gzip]
  (count-file-rows-async filepath gzip))
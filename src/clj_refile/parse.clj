(ns clj-refile.parse
  (:require [clojure.java.io :as io]
            [clojure.string :as string]
            [selmer.parser :as selmer])
  (:import (java.util.regex Pattern)
           (java.io File)
           (java.net URL URI)))

(defn pad-left [field width & [pad]]
  (string/join (concat (take (- width (count field)) (repeat (or pad " "))) field)))

(defn update-win-style-paths [p] (string/replace p "\\" "/"))

(defn append-dir-to-files
  [dir files]
  (doall
   (map
    #(string/join "/" [dir %])
    files)))

(defn get-filename-from-path
  "Return file name from any path"
  [filepath]
  (.getName (io/file filepath)))

(defn get-dir-from-path
  "Return file directory from any path"
  [filepath]
  (-> filepath io/file .getParent update-win-style-paths))

(defn get-file-pattern
  "Return a file pattern to be used for file-matching."
  [filepath]
  (-> (io/file filepath)
      .getName
      (string/replace "." "\\.")
      (string/replace "*" ".*")
      Pattern/compile))

(defn get-file-matches
  [file-pattern files]
  (vec
   (for [f files :when (re-matches
                        file-pattern
                        (if (instance? File f) (.getName f) f))]
     (if (instance? File f) (str f) f))))

(defn get-file-extension [filepath]
  (let [i (.lastIndexOf filepath ".")]
    (when (pos? i)
      (subs filepath (inc i)))))

(defn lower-case
  "nil safe lower-case method."
  [input-str]
  (when input-str (string/lower-case input-str)))

(defn- render-simple
  "Wrapper around Selmer rendered to handle errors encountered. Simple version that performs no additional replaces."
  [template input]
  (try
    (selmer/render template input)
    (catch Exception e
      (throw (Exception. (str "Error during Selmer rendering:\n" e))))))

(defn replace-conn-props-in-path
  [remote-path conn-props]
  (render-simple remote-path conn-props))

(defn parse-url
  "Parse URL string to map"
  [u]
  (try
    (let [url (URL. (str u))]
      {:protocol  (-> url .getProtocol lower-case)
       :authority (-> url .getAuthority)
       :host      (-> url .getHost)
       :port      (-> url .getPort)
       :path      (-> url .getPath)
       :query     (-> url .getQuery)
       :filename  (-> url .getFile)
       :ref       (-> url .getRef)})
    (catch Exception e
      (throw (Exception. (str "Error parsing URL (try parsing URI): " e))))))

(defn- str->uri [v]
  (try
    (URI. (str v))
    (catch Exception _ nil)))

(defn- str->file->uri [v]
  (-> v io/file (.toURI)))

(defn parse-uri
  "Parse URI string to map"
  [u]
  (let [uri (or (str->uri u) (str->file->uri u))]
    {:scheme    (-> uri .getScheme lower-case)
     :authority (-> uri .getAuthority)
     :host      (-> uri .getHost)
     :port      (-> uri .getPort)
     :path      (-> uri .getPath)
     :query     (-> uri .getRawQuery)
     :fragment  (-> uri .getFragment)}))

(defn parse-transient
  "Parse transient URI path into generic structure."
  [u]
  (let [uri (parse-uri u)
        scheme (:scheme uri)]
    (if (and scheme (some #(= (string/lower-case scheme) %) ["http" "https"]))
      {:bucket   (as->
                  (:host uri) h
                   (string/split h #"\.")
                   (get h 0))
       :path     (as->
                  (:path uri) p
                   (string/split p #"\/")
                   (remove string/blank? p)
                   (vec p)
                   (string/join "/" p))
       :filename (last (string/split (:path uri) #"\/"))}
      (let [[bucket & remaining] (as->
                                  (:path uri) p
                                   (string/split p #"\/")
                                   (remove string/blank? p)
                                   (vec p))
            path (string/join "/" remaining)]
        {:bucket bucket
         :path path
         :filename (last remaining)}))))

(ns clj-refile.providers.ftp
  (:require [clj-refile.parse :as p]
            [clj-refile.protocols :refer [FileProtocol]]
            [miner.ftp :as ftp]))

(defn- ftp-connect
  [conn-props filepath input-func]
  (try
    (let [remote-dir (-> filepath
                         (p/replace-conn-props-in-path conn-props)
                         p/get-dir-from-path)]
      (ftp/with-ftp
        [_ remote-dir
         ; possible this only works on transfers
         :file-type :binary]
        (input-func)))
    (catch Exception e
      (throw (Exception. (str "Error occurred during FTP operation: " e))))))

(defrecord FTPFile [scheme filepath conn-props]
  FileProtocol
  (delete-file [_]
    (ftp-connect
     conn-props
     filepath
     (partial
      (fn [fp client]
        (ftp/client-delete client (p/get-filename-from-path fp)))
      filepath))
    true)
  (delete-if-exists [this]
    (when (.exists? this) (.delete-file this)))
  (download [_ output]
    (.make-parents output)
    (ftp-connect
     conn-props
     filepath
     (partial
      (fn [rp lp client]
        (let [remote-filename (p/get-filename-from-path rp)]
          (ftp/client-get client remote-filename lp)
          lp))
      filepath
      (:filepath output)))
    (:filepath output))
  (exists? [_]
    (ftp-connect
     conn-props
     filepath
     (partial
      (fn [fp client]
        (let [filename (p/get-filename-from-path fp)]
          (contains? (set (ftp/client-file-names client)) filename)))
      filepath)))
  (list-files [_]
    (ftp-connect
     conn-props
     filepath
     (partial
      (fn [fp client]
        (let [uri-path (:path (p/parse-uri fp))
              file-pattern (p/get-file-pattern
                            (p/get-filename-from-path uri-path))]
          (->> (ftp/client-file-names client)
               (p/get-file-matches file-pattern)
               (p/append-dir-to-files (p/get-dir-from-path fp)))))
      filepath)))
  (upload [_ input]
    (ftp-connect
     conn-props
     conn-props
     (partial
      (fn [lp rp client]
        (let [remote-filename (p/get-filename-from-path rp)]
          (ftp/client-put client lp remote-filename)
          rp))
      (:filepath input)
      filepath))
    filepath))
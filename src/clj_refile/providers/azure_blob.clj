(ns clj-refile.providers.azure-blob
  (:require [clj-refile.protocols :refer [FileProtocol]]
            [clj-refile.providers.extras.azure-credentials :as ac]
            [taoensso.timbre :as log])
  (:import (com.azure.storage.blob BlobClientBuilder BlobClient)))

(defn- create-blob-client
  [conn-props filepath]
  (let [azf (ac/uri->azure-parsed filepath conn-props)]
    (log/debug "Creating Azure Blob Client")
    (-> (BlobClientBuilder.)
        (.endpoint (:endpoint azf))
        (ac/authenticate azf)
        (.containerName (:file-container azf))
        (.blobName (:file-location azf))
        (.buildClient))))

(defrecord AzureBlobFile [scheme filepath conn-props]
  FileProtocol
  (delete-file [_]
    (let [^BlobClient client (create-blob-client conn-props filepath)]
      (.delete client))
    true)
  (delete-if-exists [this]
    (when (.exists? this) (.delete-file this)))
  (download [_ output]
    (let [^BlobClient client (create-blob-client conn-props filepath)]
      (.make-parents output)
      (.delete-if-exists output)
      (.downloadToFile client (:filepath output)))
    (:filepath output))
  (exists? [_]
    (let [^BlobClient client (create-blob-client conn-props filepath)]
      (.exists client)))
  (list-files [this]
    (when (.exists? this)
      [filepath]))
  (upload [_ input]
    (let [^BlobClient client (create-blob-client conn-props filepath)]
      (.uploadFromFile client (:filepath input)))
    filepath))
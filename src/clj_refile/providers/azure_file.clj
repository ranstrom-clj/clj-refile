(ns clj-refile.providers.azure-file
  (:require [clj-refile.protocols :refer [FileProtocol]]
            [clj-refile.providers.extras.azure-credentials :as ac]
            [taoensso.timbre :as log])
  (:import (com.azure.storage.file.share ShareFileClientBuilder ShareFileClient)))

(defn- create-file-client
  [conn-props filepath]
  (let [azf (ac/uri->azure-parsed filepath conn-props)]
    (log/debug "Creating Azure Blob File Client")
    (-> (ShareFileClientBuilder.)
        (.endpoint (:endpoint azf))
        (ac/authenticate azf)
        (.shareName (:file-container azf))
        (.resourcePath (:file-location azf))
        (.buildFileClient))))

(defrecord AzureFile [scheme filepath conn-props]
  FileProtocol
  (delete-file [_]
    (let [^ShareFileClient client (create-file-client conn-props filepath)]
      (.delete client))
    true)
  (delete-if-exists [this]
    (when (.exists? this) (.delete-file this)))
  (download [_ output]
    (let [^ShareFileClient client (create-file-client conn-props filepath)]
      (.make-parents output)
      (.delete-if-exists output)
      (.downloadToFile client (:filepath output)))
    (:filepath output))
  (exists? [_]
    (let [^ShareFileClient client (create-file-client conn-props filepath)]
      (.exists client)))
  (list-files [this]
    (when (.exists? this)
      [filepath]))
  (upload [_ input]
    (let [^ShareFileClient client (create-file-client conn-props filepath)]
      (.create client 1024)
      (when (.uploadFromFile client (:filepath input))
        filepath))))

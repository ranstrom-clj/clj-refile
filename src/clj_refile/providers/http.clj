(ns clj-refile.providers.http
  (:require [clj-http.client :as client]
            [clj-refile.protocols :refer [FileProtocol]]
            [clojure.java.io :as io]
            [taoensso.timbre :as log])
  (:import (java.io File)))

;(def valid-auth-types ["none" "basic" "digest" "ntlm" "oauth"])

(defn- get-auth-from-properties
  [conn-props]
  (let [auth_type (-> conn-props :auth_type keyword)]
    (case auth_type
      :none nil
      :basic {:basic-auth [(:user conn-props)
                           (:pswd conn-props)]}
      :digest {:digest-auth [(:user conn-props)
                             (:pswd conn-props)]}
      :ntlm {:ntlm-auth [(:user conn-props)
                         (:pswd conn-props)
                         (:host conn-props)
                         (:domain conn-props)]}
      :oauth {:oauth-token (:token conn-props)}
      (throw (Exception. (str "Invalid auth_type: " auth_type))))))

(defn- url->byte-array [filepath]
  (let [dest (java.io.ByteArrayOutputStream.)]
    (with-open [src (io/input-stream filepath)]
      (io/copy src dest))
    (.toByteArray dest)))

(defrecord HTTPFile [scheme filepath conn-props]
  FileProtocol
  (download [_ output]
    (.make-parents output)
    (if conn-props
      (io/copy (:body (client/get filepath (merge {:as :stream}
                                                  (get-auth-from-properties conn-props))))
               (File. (:filepath output)))
      (spit (:filepath output) (-> filepath url->byte-array (String. "utf-8"))))
    (:filepath output))
  (exists? [_]
    (try
      (or (url->byte-array filepath)
          (client/get filepath (get-auth-from-properties conn-props)))
      true
      (catch Exception _
        false)))
  (list-files [this]
    (log/warn "Listing files not supported for http/https. Returning filepath if file exists.")
    (when (.exists? this)
      [filepath])))
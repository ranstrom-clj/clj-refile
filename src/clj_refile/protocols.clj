(ns clj-refile.protocols)

(defprotocol FileProtocol
  (checksum [this] "Return checksum for file")
  (copy [this output] "Perform a copy operation (requires output to be of same scheme)")
  (delete-file [this] "Delete file from location")
  (delete-if-exists [this] "Delete file if it exists")
  (download [this output] "Download file from location to output location")
  (exists? [this] "Determine if file exists")
  (list-files [this] "List all files at location")
  (make-parents [this] "Create parent directories to file")
  (upload [this input] "Upload file to location from input location"))
(ns clj-refile.formats.gzip
  (:require [clojure.java.io :as io])
  (:import (java.util.zip GZIPInputStream GZIPOutputStream)))

(defn get-reader
  [input-path gzip]
  (if gzip
    (-> input-path
        io/input-stream
        GZIPInputStream.
        io/reader)
    (-> input-path
        io/input-stream
        io/reader)))

(defn get-writer
  [output-path gzip]
  (if gzip
    (-> output-path
        io/output-stream
        GZIPOutputStream.
        io/writer)
    (-> output-path
        io/output-stream
        io/writer)))

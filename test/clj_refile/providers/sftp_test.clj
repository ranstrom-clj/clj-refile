(ns clj-refile.providers.sftp-test
  (:require [clj-refile.providers.sftp :as fh-sftp]
            [clojure.test :refer [deftest is testing]]))

(deftest test-get-connection-info
  (testing "get-connection-info"
    (let [func #'fh-sftp/get-connection-info]
      (is (= (func {:user "elrond" :pswd "no-orcs" :port 22})
             {:username                 "elrond" :password "no-orcs" :port 22
              :strict-host-key-checking :no}))
      (is (= (func {})
             {:strict-host-key-checking :no})))))
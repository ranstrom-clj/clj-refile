(ns clj-refile.providers.http-test
  (:require [clojure.test :refer [deftest is testing]]
            [clj-refile.providers.http :as fh-http]))

(deftest test-get-auth-from-properties
  (testing "get-auth-from-properties"
    (let [func #'fh-http/get-auth-from-properties]
      (is (nil? (func {:auth_type "none"})))
      (is (= (func {:auth_type "basic" :user "darth" :pswd "vader"})
             {:basic-auth ["darth" "vader"]}))
      (is (= (func {:auth_type "digest" :user "darth" :pswd "vader"})
             {:digest-auth ["darth" "vader"]}))
      (is (= (func {:auth_type "ntlm" :user "darth" :pswd "vader" :host "host.com" :domain "domain"})
             {:ntlm-auth ["darth" "vader" "host.com" "domain"]}))
      (is (= (func {:auth_type "oauth" :token "my-secret-token"})
             {:oauth-token "my-secret-token"}))
      (is (thrown? Exception (func {:auth_type "unknown"}))))))